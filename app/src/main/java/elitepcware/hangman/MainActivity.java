package elitepcware.hangman;

import android.app.Activity;
import android.content.res.AssetManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;


public class MainActivity extends Activity {
    String currentlySelectedLetter = "a";
    HangmanGame game;
    int mistakes = 0; //number of user errors
    String output = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String[] alpha = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

        //create my objects
        final TextView hiddenW = (TextView)findViewById(R.id.hiddenWord);
        final TextView character = (TextView)findViewById(R.id.selectedLetter);
        final SeekBar seekB = (SeekBar)findViewById(R.id.seekBar);
        final Button makeGuess = (Button)findViewById(R.id.guess);
        final ImageView image = (ImageView)findViewById(R.id.imageView);

        //load the dictionary file into memory and store it as a vector, if there is an error it will be
        //caught rather than having the entire program crash.
        try {
            int wordNum = 0; //number of words read from file
            Vector<String> wordlist = new Vector<String>(25143);
           BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("dic1.txt")));
            String mLine = reader.readLine();
            while (mLine != null) {
                mLine = mLine.replace("\'", "");
                wordlist.addElement(mLine);
                wordNum++;
                mLine = reader.readLine();
            }
            //create game object
            game = new HangmanGame();
            game.setWords(wordlist);
            game.selectWord(wordNum);

            for(int i = 0; i < game.getSelected().length(); i++){
                output = output + "#";
            }
            hiddenW.setText(output);
        } catch (IOException e) {
            hiddenW.setText("ERROR!");
            e.printStackTrace();
        }

        makeGuess.setOnClickListener(new View.OnClickListener() {
            @Override
            //user makes a guess and clicks the button
            public void onClick(View v) {
                if (game.letterInSelected(currentlySelectedLetter.charAt(0))){
                    output = "";
                    for (int i = 0; i < game.getSelected().length(); i++){
                        if (game.getSelected().charAt(i) == currentlySelectedLetter.charAt(0)){
                            output = output + currentlySelectedLetter;
                        }
                        else if (hiddenW.getText().charAt(i) != '#'){
                            output = output + hiddenW.getText().charAt(i);
                        }
                        else
                            output = output + "#";
                    }
                    hiddenW.setText(output);
                    //did player win?
                    if (game.sameAsSelected(hiddenW.getText().toString())){
                        image.setImageResource(R.drawable.splash);
                        makeGuess.setVisibility(View.GONE);
                        seekB.setVisibility(View.GONE);
                        character.setVisibility(View.GONE);
                    }
                }
                else{
                    //user made a mistake
                    mistakes++;
                    if (mistakes < 11){
                        //set hangman image
                        if (mistakes == 1)
                            image.setImageResource(R.drawable.c2);
                        if (mistakes == 2)
                            image.setImageResource(R.drawable.c3);
                        if (mistakes == 3)
                            image.setImageResource(R.drawable.c4);
                        if (mistakes == 4)
                            image.setImageResource(R.drawable.c5);
                        if (mistakes == 5)
                            image.setImageResource(R.drawable.c6);
                        if (mistakes == 6)
                            image.setImageResource(R.drawable.c7);
                        if (mistakes == 7)
                            image.setImageResource(R.drawable.c8);
                        if (mistakes == 8)
                            image.setImageResource(R.drawable.c9);
                        if (mistakes == 9)
                            image.setImageResource(R.drawable.c11);
                        if (mistakes == 10)
                            image.setImageResource(R.drawable.c12);
                    }
                    else{
                        //player loses
                        image.setImageResource(R.drawable.lose);
                        makeGuess.setVisibility(View.GONE);
                        seekB.setVisibility(View.GONE);
                        character.setVisibility(View.GONE);
                        hiddenW.setText(game.getSelected());

                    }
                }
            }
        });

        seekB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                character.setText(alpha[progress]);
                currentlySelectedLetter = alpha[progress];
        }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

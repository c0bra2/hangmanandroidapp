package elitepcware.hangman;

import java.util.Vector;

/**
 * Created by Jacob Mason on 2/8/2015.
 */
public class HangmanGame {
    private String selectedWord;
    private Vector<String> selectWords;
    public HangmanGame() {

    }

    public void setWords(Vector<String> words){
            selectWords = words;
    }

    public void selectWord(int range) {
            int randomNum;
            randomNum = 0 + (int) (Math.random() * range);
            selectedWord = selectWords.get(randomNum);
    }
    public String getSelected(){
        return selectedWord;
    }

    public boolean sameAsSelected(String input){

        for (int i = 0; i < selectedWord.length(); i++)
        {
            if (selectedWord.charAt(i) != input.charAt(i)){
                return false;
            }
        }
        return true;
    }

    public boolean letterInSelected(char letter) {
        for (int i = 0; i < selectedWord.length(); i++) {
            if (selectedWord.charAt(i) == letter){
                return true;
            }
        }
        return false;
    }
}
